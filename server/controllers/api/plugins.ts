import * as express from 'express'
import { getFormattedObjects } from '../../helpers/utils'
import { asyncMiddleware } from '../../middlewares/async'
import { setDefaultPagination } from '../../middlewares/pagination'
import { setDefaultSort } from '../../middlewares/sort'
import { pluginGetLatestVersionValidator, pluginsListValidator } from '../../middlewares/validators/instances'
import { paginationValidator } from '../../middlewares/validators/pagination'
import { pluginsSortValidator } from '../../middlewares/validators/sort'
import { PluginModel } from '../../models/plugin'
import { PeertubePluginLatestVersionRequest } from '../../../PeerTube/shared/models/plugins/peertube-plugin-latest-version.model'

const pluginsRouter = express.Router()

pluginsRouter.get('/',
  pluginsListValidator,
  paginationValidator,
  pluginsSortValidator,
  setDefaultSort,
  setDefaultPagination,
  asyncMiddleware(listPlugins)
)

pluginsRouter.post('/latest-version',
  pluginGetLatestVersionValidator,
  asyncMiddleware(getLatestPluginVersion)
)

// ---------------------------------------------------------------------------

export {
  pluginsRouter
}

// ---------------------------------------------------------------------------

async function listPlugins (req: express.Request, res: express.Response) {
  const pluginType = req.query.pluginType
  const currentPeerTubeEngine = req.query.currentPeerTubeEngine
  const search = req.query.search

  const resultList = await PluginModel.listForApi({
    start: req.query.start,
    count: req.query.count,
    sort: req.query.sort,
    search,
    pluginType,
    currentPeerTubeEngine
  })

  return res.json(getFormattedObjects(resultList.data, resultList.total))
}

async function getLatestPluginVersion (req: express.Request, res: express.Response) {
  const body: PeertubePluginLatestVersionRequest = req.body

  const npmNames = body.npmNames
  const currentPeerTubeEngine = body.currentPeerTubeEngine

  const result = await PluginModel.getLatestVersions(npmNames, currentPeerTubeEngine)

  return res.json(result)
}
