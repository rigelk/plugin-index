import * as express from 'express'
import { badRequest } from '../../helpers/utils'
import { pluginsRouter } from './plugins'

const apiRouter = express.Router()

apiRouter.use('/plugins', pluginsRouter)
apiRouter.use('/ping', pong)
apiRouter.use('/*', badRequest)

// ---------------------------------------------------------------------------

export { apiRouter }

// ---------------------------------------------------------------------------

function pong (req: express.Request, res: express.Response) {
  return res.send('pong').status(200).end()
}
