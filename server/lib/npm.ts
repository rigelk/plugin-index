import { doRequest } from '../helpers/requests'

interface NPMObject {
  package: {
    name: string
    version: string
    description: string

    links: {
      homepage: string
    }
  }

  score: {
    detail: {
      popularity: number
    }
  }
}

async function searchOnNPM (keywordsArg: string, options: { from: number, size: number }) {
  const keywords = encodeURIComponent(keywordsArg).replace('%2B', '+')

  const uri = `https://registry.npmjs.org/-/v1/search?text=keywords:${keywords}&size=${options.size}&from=${options.from}`

  const { body } = await doRequest({ uri, json: true })

  return body as { objects: NPMObject[] }
}

async function getPackage (npmName: string, version: string) {
  const uri = `https://registry.npmjs.org/${npmName}/${version}`

  const { body } = await doRequest({ uri, json: true })

  return body
}

export {
  NPMObject,
  searchOnNPM,
  getPackage
}
