import { SCHEDULER_INTERVAL } from '../initializers/constants'
import { logger } from '../helpers/logger'
import { PluginType } from '../../PeerTube/shared/models/plugins/plugin.type'
import { PluginModel } from '../models/plugin'
import { PluginVersionModel } from '../models/plugin-version'
import { getPackage, NPMObject, searchOnNPM } from './npm'
import { isPluginVersionValid, sanitizeAndCheckPackageJSONValid } from '../helpers/custom-validators/plugins'
import { PluginPackageJson } from '../../PeerTube/shared/models/plugins/plugin-package-json.model'

export class RequestsScheduler {

  private interval: NodeJS.Timer
  private isRunning = false

  private static instance: RequestsScheduler

  private constructor () { }

  enable () {
    logger.info('Enabling request scheduler.')

    this.interval = setInterval(() => this.execute(), SCHEDULER_INTERVAL)

    this.execute()
  }

  disable () {
    clearInterval(this.interval)
  }

  async execute () {
    // Already running?
    if (this.isRunning === true) return

    this.isRunning = true

    logger.info('Running NPM search scheduler.')

    logger.info('Searching PeerTube themes.')
    await this.scrape(PluginType.THEME)

    logger.info('Searching PeerTube plugins.')
    await this.scrape(PluginType.PLUGIN)

    this.isRunning = false
  }

  private async scrape (type: PluginType) {
    const keywords = type === PluginType.PLUGIN
      ? 'peertube+plugin'
      : 'peertube+theme'

    let from = 0
    const options = {
      detailed: true,
      size: 50,
      from: undefined
    }

    let objects: any[] = []

    do {
      options.from = from

      const res = await searchOnNPM(keywords, options)
      objects = res.objects

      await this.addPlugins(objects, type)

      from += options.size
    } while (objects.length !== 0)
  }

  private async addPlugins (objects: NPMObject[], pluginType: PluginType) {
    for (const o of objects) {
      const npmPackage = o.package

      logger.info({ object: o }, 'Adding plugin/theme %s.', npmPackage.name)

      try {
        if (await PluginModel.doesPluginVersionExist(npmPackage.name, npmPackage.version)) continue

        const packageJSON: PluginPackageJson = await getPackage(npmPackage.name, npmPackage.version)
        if (!sanitizeAndCheckPackageJSONValid(packageJSON, pluginType)) {
          logger.error({ packageJSON }, 'Cannot add plugin %s@%s because packageJSON is not valid.', npmPackage.name, npmPackage.version)
          continue
        }

        const minPeerTubeEngine = this.getMinPeerTubeSplittedVersion(packageJSON.engine.peertube)
        if (!minPeerTubeEngine) {
          logger.error({ packageJSON }, 'Invalid PeerTube engine in package.json.')
          continue
        }

        const version = this.getSplittedVersion(npmPackage.version)

        const [ plugin ] = await PluginModel.upsert({
          npmName: npmPackage.name,
          type: pluginType,
          popularity: o.score.detail.popularity * 100,
          description: packageJSON.description,
          homepage: packageJSON.homepage
        }, { returning: true })

        await PluginVersionModel.findOrCreate({
          where: {
            pluginId: plugin.id,
            rawVersion: npmPackage.version
          },
          defaults: {
            pluginId: plugin.id,
            rawVersion: npmPackage.version,
            version,
            rawPeerTubeEngine: packageJSON.engine.peertube,
            minPeerTubeEngine
          }
        })
      } catch (err) {
        logger.error({ err }, 'Cannot add plugin %s in database.', npmPackage.name)
      }
    }
  }

  private getMinPeerTubeSplittedVersion (peertubeEngine: string) {
    const min = peertubeEngine.replace(/^>=/, '')

    if (!isPluginVersionValid(min)) return undefined

    return this.getSplittedVersion(min)
  }

  private getSplittedVersion (version: string) {
    return version.split('.')
              .map(p => parseInt(p, 10))
  }

  static get Instance () {
    return this.instance || (this.instance = new this())
  }
}
