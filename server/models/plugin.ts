import { AllowNull, Column, CreatedAt, Default, HasMany, Model, Table, UpdatedAt } from 'sequelize-typescript'
import { getSort } from './utils'
import { PluginType } from '../../PeerTube/shared/models/plugins/plugin.type'
import { PeerTubePluginIndex } from '../../PeerTube/shared/models/plugins/peertube-plugin-index.model'
import { literal, Op, Order, WhereOptions } from 'sequelize'
import { Literal } from 'sequelize/types/lib/utils'
import { PluginVersionModel } from './plugin-version'
import { PeertubePluginLatestVersionResponse } from '../../PeerTube/shared/models/plugins/peertube-plugin-latest-version.model'

@Table({
  tableName: 'plugin',
  indexes: [
    {
      fields: [ 'npmName' ],
      unique: true
    }
  ]
})
export class PluginModel extends Model<PluginModel> {

  @AllowNull(false)
  @Column
  npmName: string

  @AllowNull(false)
  @Column
  type: PluginType

  @AllowNull(false)
  @Column
  popularity: number

  @AllowNull(false)
  @Column
  description: string

  @AllowNull(false)
  @Column
  homepage: string

  @AllowNull(false)
  @Default(false)
  @Column
  blacklisted: boolean

  @CreatedAt
  createdAt: Date

  @UpdatedAt
  updatedAt: Date

  @HasMany(() => PluginVersionModel, {
    foreignKey: {
      allowNull: false
    },
    onDelete: 'cascade'
  })
  Plugins: PluginModel[]

  static listForApi (options: {
    start: number,
    count: number,
    sort: string,
    search?: string,
    pluginType?: PluginType,
    currentPeerTubeEngine?: string
  }) {
    const and: WhereOptions[] = [
      {
        blacklisted: false
      }
    ]

    const query = {
      attributes: {
        include: [ ]
      },
      offset: options.start,
      limit: options.count,
      order: getSort(options.sort, [ 'id', 'ASC' ]) as Order
    }

    if (options.pluginType) {
      and.push({
        type: options.pluginType
      })
    }

    query.attributes.include.push([
      PluginModel.buildLatestVersion(options.currentPeerTubeEngine), 'latestVersion'
    ] as [ Literal, string ])

    if (options.currentPeerTubeEngine) {
      const sanitized = this.sanitizeVersion(options.currentPeerTubeEngine)

      and.push(
        literal(
          `EXISTS (SELECT 1 FROM "pluginVersion" ` +
          `WHERE "minPeerTubeEngine" <= string_to_array('${sanitized}', '.')::int[] ` +
          `AND "pluginVersion"."pluginId" = "PluginModel"."id")`
        )
      )
    }

    if (options.search) {
      const search = '%' + options.search + '%'

      and.push({
        [Op.or]: [
          {
            npmName: {
              [Op.iLike]: search
            }
          },
          {
            description: {
              [Op.iLike]: search
            }
          }
        ]
      })
    }

    Object.assign(query, { where: and })

    return PluginModel.findAndCountAll(query)
      .then(({ rows, count }) => {
        return {
          data: rows,
          total: count
        }
      })
  }

  static doesPluginVersionExist (npmName: string, version: string) {
    const query = {
      attributes: [ 'id' ],
      where: {
        npmName
      },
      include: [
        {
          model: PluginVersionModel,
          required: true,
          where: {
            rawVersion: version
          }
        }
      ]
    }

    return PluginModel.findOne(query)
      .then(p => !!p)
  }

  static getLatestVersions (npmNames: string[], currentPeerTubeEngine: string): PeertubePluginLatestVersionResponse {
    const query = {
      raw: true,
      attributes: [
        'npmName',
        [ PluginModel.buildLatestVersion(currentPeerTubeEngine), 'latestVersion' ] as [ Literal, string ]
      ],
      where: {
        npmName: {
          [Op.in]: npmNames
        }
      }
    }

    return PluginModel.findAll(query) as any
  }

  toFormattedJSON (): PeerTubePluginIndex {
    return {
      npmName: this.npmName,
      description: this.description,
      homepage: this.homepage,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt,

      popularity: this.popularity,
      latestVersion: this.get('latestVersion') as string
    }
  }

  private static sanitizeVersion (version: string) {
    return version.split('-')[0]
  }

  private static buildLatestVersion (maxPTVersion?: string) {
    let whereAnd = ''

    if (maxPTVersion) {
      // We don't need the -rc.1, -beta etc par
      const sanitized = this.sanitizeVersion(maxPTVersion)

      whereAnd = `AND "minPeerTubeEngine" <= string_to_array('${sanitized}', '.')::int[] `
    }

    return literal('(SELECT "rawVersion" FROM "pluginVersion" WHERE "pluginVersion"."pluginId" = "PluginModel"."id" ' +
      whereAnd +
      'ORDER BY "version" DESC LIMIT 1)')
  }
}
