import { AllowNull, BelongsTo, Column, CreatedAt, ForeignKey, Model, Table, UpdatedAt } from 'sequelize-typescript'
import { PluginModel } from './plugin'
import { DataTypes } from 'sequelize'

@Table({
  tableName: 'pluginVersion',
  indexes: [
    {
      fields: [ 'pluginId' ]
    },
    {
      fields: [ 'pluginId', 'version' ],
      unique: true
    }
  ]
})
export class PluginVersionModel extends Model<PluginVersionModel> {

  @AllowNull(false)
  @Column
  rawVersion: string

  @AllowNull(false)
  @Column(DataTypes.ARRAY(DataTypes.INTEGER))
  version: number[]

  @AllowNull(false)
  @Column
  rawPeerTubeEngine: string

  @AllowNull(false)
  @Column(DataTypes.ARRAY(DataTypes.INTEGER))
  minPeerTubeEngine: number[]

  @CreatedAt
  createdAt: Date

  @UpdatedAt
  updatedAt: Date

  @ForeignKey(() => PluginModel)
  @Column
  pluginId: number

  @BelongsTo(() => PluginModel, {
    foreignKey: {
      allowNull: false
    },
    onDelete: 'cascade'
  })
  Plugin: PluginModel

}
