import * as validator from 'validator'
import { isTestInstance } from '../../core-utils'
import { exists } from '../misc'

function isUrlValid (url: string) {
  const isURLOptions = {
    require_host: true,
    require_tld: true,
    require_protocol: true,
    require_valid_protocol: true,
    protocols: [ 'http', 'https' ]
  }

  // We validate 'localhost', so we don't have the top level domain
  if (isTestInstance()) {
    isURLOptions.require_tld = false
  }

  return exists(url) && validator.isURL('' + url, isURLOptions)
}

export {
  isUrlValid
}
