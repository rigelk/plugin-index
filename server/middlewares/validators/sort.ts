import { SORTABLE_COLUMNS } from '../../initializers/constants'
import { checkSort, createSortableColumns } from './utils'

const SORTABLE_PLUGINS_COLUMNS = createSortableColumns(SORTABLE_COLUMNS.PLUGINS)

const pluginsSortValidator = checkSort(SORTABLE_PLUGINS_COLUMNS)

// ---------------------------------------------------------------------------

export {
  pluginsSortValidator
}
