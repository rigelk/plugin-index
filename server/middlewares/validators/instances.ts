import * as express from 'express'
import { body, query } from 'express-validator'
import { logger } from '../../helpers/logger'
import { areValidationErrors } from './utils'
import { arePluginNpmNamesValid, isPluginTypeValid, isPluginVersionValid } from '../../helpers/custom-validators/plugins'
import { exists } from '../../helpers/custom-validators/misc'

const pluginsListValidator = [
  query('pluginType')
    .optional()
    .custom(isPluginTypeValid).withMessage('Should have a valid plugin type'),
  query('currentPeerTubeEngine')
    .optional()
    .custom(isPluginVersionValid).withMessage('Should have a valid current peertube engine'),
  query('search')
    .optional()
    .custom(exists).withMessage('Should have a valid search'),

  (req: express.Request, res: express.Response, next: express.NextFunction) => {
    logger.debug('Checking plugins list parameters', { parameters: req.query })

    if (areValidationErrors(req, res)) return

    return next()
  }
]

const pluginGetLatestVersionValidator = [
  body('npmNames')
    .custom(arePluginNpmNamesValid)
    .withMessage('Should have valid npm name'),
  body('currentPeerTubeEngine')
    .optional()
    .custom(isPluginVersionValid).withMessage('Should have a valid current peertube engine'),

  (req: express.Request, res: express.Response, next: express.NextFunction) => {
    logger.debug('Checking pluginGetLatestVersionValidator parameters', { parameters: req.body })

    if (areValidationErrors(req, res)) return

    return next()
  }
]

// ---------------------------------------------------------------------------

export {
  pluginsListValidator,
  pluginGetLatestVersionValidator
}
