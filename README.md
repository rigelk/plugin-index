# Application behind packages.joinpeertube.org

## Dev

```terminal
$ git submodule update --init --recursive
$ yarn install --pure-lockfile
```

Initialize the database:

```terminal
$ sudo -u postgres createuser -P peertube
$ sudo -u postgres createdb -O peertube peertube_plugin_index
```

Then run simultaneously (for example with 2 terminals):

```terminal
$ tsc -w
```

```terminal
$ node dist/server
```

Then open http://localhost:3233.

## Production

In the root of the cloned repo:

```terminal
$ git submodule update --init --recursive
$ yarn install --pure-lockfile
$ npm run build
$ node dist/server.js
```
